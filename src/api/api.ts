import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';

import { HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from 'rxjs';

let BASE_URL = 'https://elshomedecor.in/api/';

@Injectable()
export class ApiProvider {
    isLoading = false;
  
    constructor(private toastCtrl: ToastController, public loadingCtrl: LoadingController,public alertCtrl: AlertController, public navctrl: NavController,private http: HttpClient) {
        
      }

      async presentAlert(msg) {
        const alert = await this.alertCtrl.create({
          message: msg,
          buttons: ['OK']
        });
    
        await alert.present();
      }
      async presentToast(msg) {
        const toast = await this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
      async present() {
        this.isLoading = true;
        return await this.loadingCtrl.create({
          // duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }
    
      async dismiss() {
        this.isLoading = false;
        return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
      }
      category():Observable<any>
      {
        let api_url =BASE_URL+ 'elshome.websitecat';

        return this.http.get(api_url);
    
      }
      login(form){
        //const formData = new FormData();
        let api_url= BASE_URL+"auth/get_tokens";
        
        var data =
        {
            "db":"elshomedecor",
            "username":"admin@elshomedecor.com",
            "password":"654321",
         }
       
         let headers = new HttpHeaders();
         headers.append('Content-Type','text/plain');
        return this.http.post(api_url, data, {headers:headers}); 
        
      }
      products(id):Observable<any>
      {
        let api_url= BASE_URL+"elshome.product.template?filters=[('public_categ_ids','=',"+parseInt(id)+")]";
        return this.http.get(api_url);
      }

      productdetail(id):Observable<any>
      {
        let api_url= BASE_URL+"elshome.product.template1/"+parseInt(id);
        return this.http.get(api_url);
      }
    }