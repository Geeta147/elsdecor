import { Component, OnInit } from '@angular/core';
import {  NavController, AlertController } from '@ionic/angular';


import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ApiProvider } from 'src/api/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  RetrievedProduct:any=[];
  AddedProductId:any;
  AddArray:any=[];
  totalAmount:number;
  NumOfItem:any;
  noproducts:boolean= false;
  quantity:any;
  initial_price:number;
  cartService: any;
  isRetrievedProduct:boolean = false;
  Removeproduct:any=[];
  //  localStorage.getItem('product');

  constructor( private nav: NavController,private router:Router,private sanitizer: DomSanitizer,private service:ApiProvider,public alertCtrl: AlertController) { 
    
    
    
    this.RetrievedProduct=JSON.parse(localStorage.getItem('productdetail'));
    //if(this.RetrievedProduct)
    if(this.RetrievedProduct==null || this.RetrievedProduct==undefined)
  {
  this.RetrievedProduct=[];
  this.isRetrievedProduct=true;
  }else{
    this.isRetrievedProduct=false;
  }
    
    this.RetrievedProduct.forEach((key) => { key["quantity"] = 1; });
    console.log(this.RetrievedProduct)
    this.recalculateTotalAmount();
  
    this.NumOfItem=this.RetrievedProduct.length;
     console.log(this.NumOfItem);
       
  }      
  ngOnInit() {
   
  
  }


  getImgContent(image): SafeUrl {
    var image1 = 'data:image/gif;base64,'+image;

  return this.sanitizer.bypassSecurityTrustUrl(image1);
  
}
clearall(){
 
window.localStorage.clear();
this.RetrievedProduct=[];
this.isRetrievedProduct=true;


}

  backhome(){
    this.nav.navigateRoot('/home')
  }

  remove(item){
   var id =item.id;
   var index= this.RetrievedProduct.find((item:any)=>item.id=== id);
    this.RetrievedProduct.splice(index,1);
    this.NumOfItem= this.RetrievedProduct.length;
  
    this.totalAmount=this.totalAmount-item.list_price;
   // this.RetrievedProduct = this.RetrievedProduct;   
    localStorage.setItem('RetrievedProduct',JSON.stringify(this.RetrievedProduct));
    this.Removeproduct.push(this.RetrievedProduct);
   
    // window.localStorage.removeItem(item.id);
    // console.log(this.Removeproduct);
    // console.log(this.RetrievedProduct);
   
     if(this.NumOfItem==0 ){
    
   // this.Removeproduct=[];
    this.isRetrievedProduct=true;
     }
    
  //  
    
  }
 

minus(i) {
  this.RetrievedProduct[i].quantity--;
  this.recalculateTotalAmount();
}
plus(i) {
  
 this.RetrievedProduct[i].quantity++;
 
 this.recalculateTotalAmount();
 
}

recalculateTotalAmount() {
  let newTotalAmount=0;
  this.RetrievedProduct.forEach( item => {
      newTotalAmount += (item.list_price * item.quantity)
  });
  this.totalAmount = newTotalAmount;
  
  console.log(this.totalAmount)
}

enterAddress(){
  console.log();
  
  this.nav.navigateRoot('/address');
}
categoryPage(id){
  this.nav.navigateRoot('/home');
}
}
