import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import { ApiProvider } from 'src/api/api';
import { ActivatedRoute, NavigationExtras } from "@angular/router";
import { HttpClient} from "@angular/common/http";
import { SafeUrl,DomSanitizer  } from '@angular/platform-browser';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  productData:any=[];
  id:any;
  sub:any;
  // addProductToCart:any[];

  constructor(private route: ActivatedRoute, private nav:NavController,private service:ApiProvider,private http: HttpClient,private sanitizer: DomSanitizer) { 
   
  //   this.route.queryParams.subscribe(params => {
     
  //     this.product = JSON.parse(params["details"]);
    
  //     console.log(this.product)
  // });
  this.sub = this.route.params.subscribe(params => {
    console.log(params)
          this.id = params['id']; 
          console.log(this.id)
     });
    
  }
 
  ngOnInit() {
    this.productdetailApi();
  }
  productdetailApi(){
    this.service.productdetail(this.id)
    .subscribe(data =>{
    console.log(data);
    this.productData=data;
    console.log(this.productData)
  
  },
  errData =>{
   console.log(errData);
  
   });
  }
 
  getImgContent(image): SafeUrl {
    var image1 = 'data:image/gif;base64,'+image;

  return this.sanitizer.bypassSecurityTrustUrl(image1);
  
}

  
  backToCategory(){
this.nav.navigateRoot('categories');
  }
  //  addToCart(){
  //   this.addProductToCart=this.product;
  //   console.log(this.addProductToCart);
  //   localStorage.setItem('product',JSON.stringify(this.addProductToCart))
  //  }
  

  moveToCart(){
    this.nav.navigateForward('cart');
  }
}
