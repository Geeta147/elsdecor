import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {
  
  constructor(private nav:NavController,private route:ActivatedRoute) { 
    
  }

  ngOnInit() {
  }

  backToCart(){
    this.nav.navigateRoot('cart');
  } 
}
