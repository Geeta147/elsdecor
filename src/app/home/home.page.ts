import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import { ApiProvider } from 'src/api/api';
import {Router} from '@angular/router';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  id:any;
images:any[];
topCategory=[];

  constructor( private router:Router, public nav:NavController,private service:ApiProvider,private sanitizer: DomSanitizer) { 
    this.init();
  }

  ngOnInit() {
    this.categoryApi();
  }
  init(){
this.images=[];
for(let i=2; i<=6; i++){
this.images.push("assets/images/"+i+".png");
}


  }
  categoryApi(){
    this.service.category()
    .subscribe(data =>{
    console.log(data);
    this.topCategory=data.results;
    console.log(this.topCategory)
    
     
    },
    errData =>{
     console.log(errData);
   
     });
  }
  getImgContent(image): SafeUrl {
      var image1 = 'data:image/gif;base64,'+image;

    return this.sanitizer.bypassSecurityTrustUrl(image1);
    
}
  category(id){
   console.log(id)
    this.router.navigate(['/categories',id]);
    //this.nav.navigateRoot('/categories');
    // this.nav.push('categories',{id:id});
    console.log(id);
  
  }
  moveToCart(){
    this.nav.navigateRoot('/cart');

  }
  
}
