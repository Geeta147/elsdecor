import { Component, OnInit } from '@angular/core';
import { ApiProvider } from '../../api/api';
import { FormGroup, FormControl, Validators } from '@angular/forms';
// import { Events } from 'ionic-angular';
import { NavController, MenuController,AlertController } from '@ionic/angular';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
//import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  selectedLanguage: string;
  myForm: FormGroup;
  // loginres: any;
  // alerts: any;
  // projectName: any;

  constructor(public service:ApiProvider,public navctrl:NavController,private alertCtrl: AlertController,private sanitizer: DomSanitizer) {
    this.myForm = new FormGroup({
      username: new FormControl(null, Validators.compose([Validators.required])),
      password: new FormControl(null, Validators.compose([Validators.required])),
    });
    // this.menuctrl.enable(false);
   }
     ngOnInit() {

     }

  onSubmit(myForm){
    console.log(myForm);
    if(this.validate()){
      this.service.present();
      this.service.login(myForm)
      .subscribe(res =>{
        this.service.dismiss();
        console.log(res);
      },
      errData =>{
        this.service.dismiss();
        if(errData.status==0){
          this.service.presentToast('No Internet Connection');
        }
        else{
          this.service.presentToast(errData.statusText);
        }
       });
      }
    }
   
   
    validate(): boolean {
    if (this.myForm.valid) {
      return true;
    }
    let errorMsg = '';
   let userName_control = this.myForm.controls['username'];
   let password_control = this.myForm.controls['password'];
  
  
   if (!userName_control.valid) {
     if (userName_control.errors['required']) {
      errorMsg ='Please enter your email';
      this.service.presentAlert(errorMsg);
      return;
        
      }
    } 
    if (!password_control.valid) {
     if (password_control.errors['required']) {
      errorMsg ='Please enter your password';
      this.service.presentAlert(errorMsg);
      return;
   
     } 
    }
  }
   backToHome(){
     this.navctrl.navigateRoot('/home');
   }
   signup(){
     this.navctrl.navigateRoot('/signup')
   }
}
