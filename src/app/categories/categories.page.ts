import { Component, OnInit } from '@angular/core';
import {NavController, ToastController, LoadingController, AlertController,NavParams} from '@ionic/angular';
import { ApiProvider } from 'src/api/api';
import {ActivatedRoute} from '@angular/router';
import { SafeUrl,DomSanitizer  } from '@angular/platform-browser';
//import { NavigationExtras } from '@angular/router';
import {Router} from '@angular/router';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  product:[];
  
  id:any;
  sub: any;
  addProductToCart:any=[];
  toast: any;
  _id;any;
  
  constructor( private router:Router,private route:ActivatedRoute, 
    public alertCtrl: AlertController,private toastCtrl: ToastController,public loadingCtrl: LoadingController,private nav: NavController,private service:ApiProvider,private sanitizer: DomSanitizer) {
    this.sub = this.route.params.subscribe(params => {
console.log(params)
      this.id = params['id']; 
      console.log(this.id)
      
   
    });

   }
   addedItem:any[];
   
  ngOnInit() {
    this.productApi();
  }
 
   
  productApi(){
    this.service.products(this.id)
    .subscribe(data =>{
    console.log(data);
    this.product=data.results;
    console.log(this.product)
   
  //   let navigationExtras: NavigationExtras = {
  //     queryParams: {
  //       details: JSON.stringify(this.product)
  //     }
  // };
  // this.nav.navigateForward(['product-details'],navigationExtras);
  
   
     
    },
    errData =>{
     console.log(errData);
   
     });
  }
  getImgContent(image): SafeUrl {
    var image1 = 'data:image/gif;base64,'+image;

  return this.sanitizer.bypassSecurityTrustUrl(image1);
  
}
backhome(){
  this.nav.navigateRoot('/home')
}
description(id){
    console.log(id)
    this.router.navigate(['/product-details',id]);
    console.log(id);
   
   
    //this.nav.navigateRoot('/categories');
    // this.nav.push('categories',{id:id});
   // console.log(id);
//   let navigationExtras: NavigationExtras = {
//          queryParams: {
//           details: JSON.stringify(productdetail)
//         }
//     }
// this.nav.navigateForward(['product-details'],navigationExtras);
    
 //this.nav.navigateRoot('product-details');


}
addToCart(productdetail){
  //console.log(productdetail.id);
  if(productdetail.id && productdetail.id != this._id)
  {
   this.addProductToCart=JSON.parse(localStorage.getItem('productdetail'));
   console.log(this.addProductToCart)
   if(this.addProductToCart==null || this.addProductToCart==undefined)
   this.addProductToCart=[];
   this.addProductToCart.push(productdetail);
   console.log(this.addProductToCart);
   
   localStorage.setItem('productdetail',JSON.stringify(this.addProductToCart));
   this._id=productdetail.id;
   console.log(this._id);
   console.log(this.addProductToCart);
   this.service.presentAlert("added to cart");
  }
   else
   {
    var msg= 'item already in cart';
    this.service.presentAlert(msg);
   }
   
   
}
 

moveToCart(){
  this.nav.navigateRoot('/cart');

}

  
}
